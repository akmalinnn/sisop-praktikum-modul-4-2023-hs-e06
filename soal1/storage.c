#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void download_file() {

    

        system("kaggle datasets download -d bryanb/fifa-player-stats-database");
        system("unzip fifa-player-stats-database.zip");

}

void analis_file() {
    system("awk -F',' 'NR==1 { printf(\"%-25s%-25s%-10s%-10s%-s\\n\", $2, $9, $3, $8, $4) } \
                  NR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { \
                  printf(\"%-25s%-25s%-10s%-10s%-s\\n\", $2, $9, $3, $8, $4) }' FIFA23_official_data.csv > list.txt");
}

int main() {
    

    download_file();
    analis_file();
    system("cat list.txt ");  // Redirect output to stdout


}
