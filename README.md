# sisop-praktikum-modul-4-2023-HS-E06


## Praktikum Modul-4 Sistem Operasi


- 5025211049	Zakia Kolbi
- 5025211210	Nadiah Nuri Aisyah
- 5025211216	Akmal Nafis

# Nomor 1

- membuat file bernama storage.c, mendownload dataset tentang pemain sepak bola di seluruh dunia dalam bentuk .zip kemudian unzip filenya. Untuk mendownload dataset dari kaggle dibutuhkan konfigurasi untuk mengatur environtmentnya. Download kaggle menggunakan python pip, kemudian download token API dari webnya dan letakkan di dalam folder root

``c
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    system("unzip fifa-player-stats-database.zip");
```
- menggunakan fungsi system untuk mendownload dan mengunzip file 
```c
system("awk -F',' 'NR==1 { printf(\"%-25s%-25s%-10s%-10s%-s\\n\", $2, $9, $3, $8, $4) } \
                  NR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { \
                  printf(\"%-25s%-25s%-10s%-10s%-s\\n\", $2, $9, $3, $8, $4) }' FIFA23_official_data.csv > list.txt");

```
- Memfilter file sesuai dengan kolomnya .csv menggunakan fungsi awk sesuai dengan ketentuan 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Kemudian cetak nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka. Disini saya menyimpan file tersebut didalam file list.txt 


```c
FROM ubuntu:20.04 
RUN apt-get update && apt-get install -y \
    build-essential \
    wget \
    unzip 
RUN apt-get update && \
    apt-get install -y python3-pip && \
    pip3 install kaggle

WORKDIR /app

COPY kaggle.json /root/.kaggle/kaggle.json  

RUN chmod 600 /root/.kaggle/kaggle.json

COPY storage.c /app

RUN gcc -o storage storage.c

# Set the command to run when the container starts
CMD ["./storage"]

```
- konfigurasi dockerfile menggunakan ubuntu update sistem terlebih dahulu, kemudian install sesuai kebutuhan dimana membutuhkan unzip, dan install phthon pip. kemudian mendownload kaggle. atur environtment untuk kaggle, copy file c dan json ke container dimana json API token yang sama dengan yang sebelumnya. Atur permission pada file kaggle.json. kemudian compile menggunakan gcc, karena ubuntu sudah terinstall gcc maka tidak perlu install gcc, kemudian jalankan program.

- beberapa contoh command yang digunakan 
```c
sudo docker build -t modul4:0.5 . //untuk membuat image
sudo docker run -it modul4:0.5  //untuk menjalankan image ke container

sudo docker images      //mengecek image yang telah dibuat
sudo docker ps -a       // mengecek container yang telah dijalankan 
sudo docker container logs  // mengecek logs ouput dari container yang telah dijalankan

// untuk mempublish image

docker login                            
docker tag modul4:0.5 akmalx/modul4:0.5  //ganti nama sesuai dengan kebutuhan 
docker push akmalx/modul4:0.5           //upload ke dockerhub

```
- Membuat docker compose, yang dijalankan pada folder napoli dan balcelona 


```c
version: '3'

services:
  storage-app-1:    //instances yang dibuat 1 sampai 5
    image: akmalx/modul4 //image yang berada pada dockerhub 
    ports:
      - "8000:8000" // gunakan port yang berbeda beda pada tiap instances 

```
- jalankan `docker compose up -d` pada tiap folder 

-
![image](https://cdn.discordapp.com/attachments/472360024975605762/1114566107030880366/image.png)
![image](https://cdn.discordapp.com/attachments/472360024975605762/1114559582132514916/image.png)
![image](https://cdn.discordapp.com/attachments/472360024975605762/1114559367509975150/image.png)
![image](https://cdn.discordapp.com/attachments/472360024975605762/1114559146591797359/image.png)

# Nomor 3
- Point A sampai E
- Membuat FUSE yang termodifikasi dengan source mount-nya adalah /etc yang sudah ditentukan
- Melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose
- Intinya tidak bisa mengerjakan.

# Nomor 4
- Point A sampai E
- Tidak bisa mengerjakan.
